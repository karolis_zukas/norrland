using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelEnableTrigger : MonoBehaviour
{
    public GameObject victoryPanel;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            //other.GetComponent<PlayerController>().enabled = false;
            //other.GetComponent<Rigidbody>().velocity = Vector2.zero;
            victoryPanel.SetActive(true);
        }
    }
}
