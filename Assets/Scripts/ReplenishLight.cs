using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReplenishLight : MonoBehaviour
{
    public float minutesIncrease;

    private void Start()
    {

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        ItemMusic itemMusic = GetComponent<ItemMusic>();

        if (other.CompareTag("Player"))
        {
            SurvivalMeter.instance.ReplenishLightBar(minutesIncrease);
            PlayerController pc = other.gameObject.GetComponentInParent<PlayerController>();

            if(pc != null)
            {
                pc.audioSource.volume = 0.5f;
                pc.audioSource.PlayOneShot(itemMusic.pickupSound);
            }

            this.gameObject.SetActive(false);
        }
    }
}
