using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    public AudioClip hitSound;
    public List<AudioClip> hitSounds = new List<AudioClip>();

    private void Start()
    {
        if (hitSounds.Count != 0)
        {
            hitSound = hitSounds[Random.Range(0, hitSounds.Count)];
        }
    }
}
