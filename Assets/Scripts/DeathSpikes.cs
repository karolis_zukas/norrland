using UnityEngine;

public class DeathSpikes : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            SurvivalMeter.instance.Death();
        }
    }
}
