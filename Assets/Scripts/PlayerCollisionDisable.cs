using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollisionDisable : MonoBehaviour
{
    public GameObject objectToDisable;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            objectToDisable.SetActive(false);
            this.gameObject.SetActive(false);
        }
    }
}
