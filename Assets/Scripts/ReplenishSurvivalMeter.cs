using UnityEngine;

public class ReplenishSurvivalMeter : MonoBehaviour
{
    public float minutesIncrease;

    private void OnTriggerEnter2D(Collider2D other)
    {
        ItemMusic itemMusic = GetComponent<ItemMusic>();

        if (other.CompareTag("Player"))
        {
            PlayerController pc = other.gameObject.GetComponentInParent<PlayerController>();

            if (pc != null)
            {
                pc.audioSource.volume = 0.5f;
                pc.audioSource.PlayOneShot(itemMusic.pickupSound);
            }

            SurvivalMeter.instance.ReplenishBar(minutesIncrease);
            this.gameObject.SetActive(false);
        }
    }
}
