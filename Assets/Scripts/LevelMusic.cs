using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelMusic : MonoBehaviour
{
    public static LevelMusic instance;

    public AudioClip lvl1;
    public AudioClip lvl2;
    public AudioClip lvl3;

    public AudioClip deathMusic;
    private bool isPlayingTrack1;

    private AudioSource audioSource1;
    private AudioSource audioSource2;

    public float lvl1Start = 0;
    public float lvl2Start = -24f;
    public float lvl3Start = -55f;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }

    private void Start()
    {
        audioSource1 = gameObject.AddComponent<AudioSource>();
        audioSource2 = gameObject.AddComponent<AudioSource>();
        SwapAudio(lvl1);
    }

    public void CheckIfNeedsChanging(float playerY)
    {
        if(playerY <= lvl2Start && playerY > lvl3Start)
        {
            SwapAudio(lvl2);
        }
        if (playerY <= lvl3Start)
        {
            SwapAudio(lvl3);
        }

    }

    public void SwapAudio(AudioClip newClip)
    {
        StopAllCoroutines();
        StartCoroutine(FadeTrack(newClip, 5f));

        isPlayingTrack1 = !isPlayingTrack1;
    }

    public void PlayDeathMusic()
    {
        StopAllCoroutines();
        StartCoroutine(FadeTrack(deathMusic, 2f));
    }

    
    private IEnumerator FadeTrack(AudioClip newClip, float timeToFade)
    {
        float timeElapsed = 0;

        if (isPlayingTrack1)
        {
            audioSource2.clip = newClip;
            audioSource2.Play();

            while(timeElapsed < timeToFade)
            {
                audioSource2.volume = Mathf.Lerp(0, 1, timeElapsed / timeToFade);
                audioSource1.volume = Mathf.Lerp(1, 0, timeElapsed / timeToFade);
                timeElapsed += Time.deltaTime;
                yield return null;
            }

            audioSource1.Stop();
        }
        else
        {
            audioSource1.clip = newClip;
            audioSource1.Play();

            while (timeElapsed < timeToFade)
            {
                audioSource1.volume = Mathf.Lerp(0, 1, timeElapsed / timeToFade);
                audioSource2.volume = Mathf.Lerp(1, 0, timeElapsed / timeToFade);
                timeElapsed += Time.deltaTime;
                yield return null;
            }

            audioSource2.Stop();
        }
    }
}
