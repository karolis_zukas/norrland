using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class SurvivalMeter : MonoBehaviour
{
    
    public static SurvivalMeter instance;
    PlayerController controller;
    public GameObject deathScreen;
    private bool routineActivated;
    private LevelMusic levelMusic;
    
    [Header("FoodSettings")]
    private float maxDuration;
    public float minutesDuration;
    private float secondsDuration;
    public Image survivalBar;

    [Header("LightSettings")]
    private float maxLightDuration;
    public float lightMinutesDuration;
    private float lightSecondsDuration;
    public Image lightBar;
    public GameObject darkness;
    

    private void Start()
    {
        maxDuration = minutesDuration * 60f;
        secondsDuration = maxDuration;
        
        maxLightDuration = lightMinutesDuration * 60f;
        lightSecondsDuration = maxLightDuration;
        
        //InvokeRepeating("DeplenishBar", 1f, 1f);
        SurvivalMeter.instance = this;
        controller = FindObjectOfType<PlayerController>();
        levelMusic = FindObjectOfType<LevelMusic>();
    }

    private void Update()
    {
        secondsDuration -= Time.deltaTime;
        survivalBar.fillAmount = secondsDuration / maxDuration;
        if (secondsDuration <= 0)
        {
            Death();
        }

        if (lightSecondsDuration > 0)
        {
            lightSecondsDuration -= Time.deltaTime;
            lightBar.fillAmount = lightSecondsDuration / maxLightDuration;
        }

        else if(routineActivated == false)
            StartCoroutine("LightsOff");
    }

    private void DeplenishBar()
    {
        secondsDuration -= 1;
        survivalBar.fillAmount = secondsDuration / maxDuration;
        if(secondsDuration <= 0)
        {
            Death();
        }

        if (lightSecondsDuration > 0)
        {
            lightSecondsDuration -= 1;
            lightBar.fillAmount = lightSecondsDuration / maxLightDuration;
        }

        else
            StartCoroutine("LightsOff");
        
        
    }

    public void ReplenishBar(float minutesIncrease)
    {
        secondsDuration += minutesIncrease * 60f;
        if(secondsDuration > maxDuration)
        {
            secondsDuration = maxDuration;
        }
        survivalBar.fillAmount = secondsDuration / maxDuration;
    }

    public void ReplenishLightBar(float lightIncrease)
    {
        if(lightSecondsDuration <= 0)
        {
            StopCoroutine("LightsOff");
            StartCoroutine("LightsOn");
        }
        lightSecondsDuration += lightIncrease * 60f;
        if(lightSecondsDuration > maxLightDuration)
        {
            lightSecondsDuration = maxLightDuration;
        }
        lightBar.fillAmount = lightSecondsDuration / maxLightDuration;
    }

    public void Death()
    {
        controller.playerAnimator.SetBool("isDead", true);
        levelMusic.PlayDeathMusic();
        
        CancelInvoke();
        controller.enabled = false;
        controller.gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        deathScreen.SetActive(true);
        this.enabled = false;
    }

    public IEnumerator LightsOff()
    {
        routineActivated = true;
        Vector2 target = new Vector2(2f, 2f);
        while (darkness.transform.localScale.x > 1.1)
        {
            darkness.transform.localScale = Vector2.Lerp(darkness.transform.localScale, target, Time.deltaTime * 0.2f);
            yield return new WaitForFixedUpdate();
        }
        routineActivated = false;
    }

    public IEnumerator LightsOn()
    {
        Vector2 target = new Vector2(6f, 6f);
        while (darkness.transform.localScale.x < 5.9f)
        {
            darkness.transform.localScale = Vector2.Lerp(darkness.transform.localScale, target, Time.deltaTime);
            yield return new WaitForFixedUpdate();
        }
        routineActivated = false;
    }



}
