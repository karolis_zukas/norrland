using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(CapsuleCollider2D))]

public class PlayerController: MonoBehaviour
{
    public float maxSpeed = 3.4f;
    public float jumpHeight = 6.5f;
    public float gravityScale = 2.5f;
    public Camera mainCamera;
    public GameObject digParticle;

    public List<AudioClip> walkAudioClips = new List<AudioClip>();
    public float digHoldDuration = .8f;

    public bool isGrounded = false;
    bool facingRight = true;
    float moveDirection = 0;
    float timer;

    public Animator playerAnimator;
    public AudioSource audioSource;
    Vector3 cameraPos;
    Rigidbody2D r2d;
    CapsuleCollider2D mainCollider;
    BoxCollider2D triggerCollider;
    Transform t;

    private float cameraStartX;
    private LevelMusic levelMusic;

    // Use this for initialization
    void Start()
    {
        levelMusic = FindObjectOfType<LevelMusic>();
        playerAnimator = GetComponentInChildren<Animator>();
        audioSource = GetComponent<AudioSource>();
        t = transform;
        cameraStartX = t.position.x;
        r2d = GetComponent<Rigidbody2D>();
        mainCollider = GetComponent<CapsuleCollider2D>();
        triggerCollider = GetComponentInChildren<BoxCollider2D>();
        r2d.freezeRotation = true;
        r2d.collisionDetectionMode = CollisionDetectionMode2D.Continuous;
        r2d.gravityScale = gravityScale;
        facingRight = t.localScale.x > 0;

        if (mainCamera)
        {
            cameraPos = mainCamera.transform.position;
        }
    }

    void Update()
    {
        if (GetHorizontalControls() && (isGrounded || Mathf.Abs(r2d.velocity.x) > 0.01f))
        {
            if (walkAudioClips.Count > 0 && !audioSource.isPlaying)
            {
                audioSource.PlayOneShot(walkAudioClips[Random.Range(0, walkAudioClips.Count)]);
            }

            moveDirection = Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow) ? -1 : 1;
            playerAnimator.SetBool("isWalking", true);

            // Dig sideways
            if (IsDigButtonPressed() && isGrounded)
            {
                timer = Time.time;
            } else if(IsDigButtonHeld())
            {
                playerAnimator.SetBool("isDigging", true);
                if (Time.time - timer > digHoldDuration)
                {
                    timer = float.PositiveInfinity;
                    DigSideways();
                    timer = Time.time;
                }
            }
        }
        else
        {
            playerAnimator.SetBool("isWalking", false);
            if (isGrounded || r2d.velocity.magnitude < 0.01f)
            {
                moveDirection = 0;
            }
        }

        if (IsDigButtonReleased())
        {
            playerAnimator.SetBool("isDigging", false);
            playerAnimator.SetBool("isDiggingDown", false);
        }

        // Dig down
        if (Input.GetKeyDown(KeyCode.S) && isGrounded)
        {
            timer = Time.time;
        }
        else if ((Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow)) && IsDigButtonHeld())
        {
            playerAnimator.SetBool("isDiggingDown", true);
            if (Time.time - timer > digHoldDuration)
            {
                timer = float.PositiveInfinity;
                DigDown();
                timer = Time.time;
            }
        }

        // Change facing direction
        if (moveDirection != 0)
        {
            if (moveDirection > 0 && !facingRight)
            {
                facingRight = true;
                t.localScale = new Vector3(Mathf.Abs(t.localScale.x), t.localScale.y, transform.localScale.z);
            }
            if (moveDirection < 0 && facingRight)
            {
                facingRight = false;
                t.localScale = new Vector3(-Mathf.Abs(t.localScale.x), t.localScale.y, t.localScale.z);
            }
        }

        // Jumping
        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {
            r2d.velocity = new Vector2(r2d.velocity.x, jumpHeight);
        }

        if (r2d.velocity.y != 0)
        {
            playerAnimator.SetBool("isJumping", true);
            playerAnimator.SetBool("isDiggingDown", false);
            playerAnimator.SetBool("isDigging", false);
        }
        else
        {
            playerAnimator.SetBool("isJumping", false);
        }


        if (mainCamera)
        {
            mainCamera.transform.position = new Vector3(cameraStartX, t.position.y, cameraPos.z);
        }
    }

    private bool IsDigButtonPressed()
    {
        return Input.GetKeyDown(KeyCode.LeftShift) || Input.GetKeyDown(KeyCode.RightShift);
    }

    private bool IsDigButtonHeld()
    {
        return Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);
    }

    private bool IsDigButtonReleased()
    {
        return Input.GetKeyUp(KeyCode.LeftShift) || Input.GetKeyUp(KeyCode.RightShift) || Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D) || Input.GetKeyUp(KeyCode.S) || Input.GetKeyUp(KeyCode.DownArrow);
    }

    void DigSideways()
    {
        RaycastHit2D hit;

        if (moveDirection > 0)
        {
            hit = Physics2D.Raycast(mainCollider.bounds.max, -Vector2.left);
        } else
        {
            hit = Physics2D.Raycast(mainCollider.bounds.min, -Vector2.right);
        }
        HitTheTile(hit);
    }

    void DigDown()
    {
        Vector2 colliderBottomCenter = new Vector2(mainCollider.bounds.center.x, mainCollider.bounds.center.y - mainCollider.bounds.size.y);
        RaycastHit2D hit = Physics2D.Raycast(colliderBottomCenter, -Vector2.up);

        HitTheTile(hit);
    }

    void HitTheTile(RaycastHit2D hit)
    {
        if (hit.collider != null && hit.distance < 0.1)
        {
            GameObject hitTile = hit.collider.transform.gameObject;

            if(hitTile.GetComponent<Tile>() != null)
            {


                AudioClip hitSound = hitTile.GetComponent<Tile>().hitSound;
                audioSource.clip = hitSound;
                audioSource.volume = 0.1f;
                audioSource.Play();
                
            }

            if (hit.collider.transform.gameObject.CompareTag("Diggable"))
            {
                digParticle.SetActive(false);
                digParticle.transform.position = hitTile.transform.position;
                digParticle.SetActive(true);
                Destroy(hitTile);

                if(transform.position.y + 1f >= levelMusic.lvl2Start && transform.position.y <= levelMusic.lvl2Start && transform.position.y > levelMusic.lvl3Start)
                {
                    levelMusic.CheckIfNeedsChanging(transform.position.y);
                } else if(transform.position.y + 1f >= levelMusic.lvl3Start && transform.position.y <= levelMusic.lvl3Start)
                {
                    levelMusic.CheckIfNeedsChanging(transform.position.y);
                }

            }

        }
    }

    bool GetHorizontalControls()
    {
        return Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow);
    }

    void FixedUpdate()
    {
        Bounds colliderBounds = mainCollider.bounds;
        float colliderRadius = mainCollider.size.x * 0.4f * Mathf.Abs(transform.localScale.x);
        Vector3 groundCheckPos = colliderBounds.min + new Vector3(colliderBounds.size.x * 0.5f, colliderRadius * 0.9f, 0);
        // Check if player is grounded
        Collider2D[] colliders = Physics2D.OverlapCircleAll(groundCheckPos, colliderRadius);
        //Check if any of the overlapping colliders are not player collider, if so, set isGrounded to true
        isGrounded = false;

        if (colliders.Length > 0)
        {
            for (int i = 0; i < colliders.Length; i++)
            {
                if (colliders[i] != mainCollider && colliders[i] != triggerCollider)
                {
                    isGrounded = true;
                    break;
                }
            }
        }

        // Apply movement velocity
        r2d.velocity = new Vector2((moveDirection) * maxSpeed, r2d.velocity.y);

        // Simple debug
        //Debug.DrawLine(groundCheckPos, groundCheckPos - new Vector3(0, colliderRadius, 0), isGrounded ? Color.green : Color.red);
        //Debug.DrawLine(groundCheckPos, groundCheckPos - new Vector3(colliderRadius, 0, 0), isGrounded ? Color.green : Color.red);
    }
}